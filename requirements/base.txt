# Django
# ------------------------------------------------------------------------------
django >=4.1,<4.2  # https://www.djangoproject.com/
django-environ >=0.9,<0.10  # https://github.com/joke2k/django-environ
django-mailer >=2.1,<2.2 # https://github.com/pinax/django-mailer
django-tapeforms

# Wagtail
# ------------------------------------------------------------------------------
wagtail >=4.1,<4.2  # https://wagtail.io
wagtailembedpeertube # https://forge.cliss21.org/cliss21/wagtailembedpeertube
wagtailmenus >=3.1,<3.2  # https://github.com/rkhleics/wagtailmenus

# Documentation
# ------------------------------------------------------------------------------
sphinx
sphinx-rtd-theme
django-docs ==0.3.3

# Utils
# ------------------------------------------------------------------------------
dnspython
