import $ from 'jquery';

// Export jQuery for external usage
window.jQuery = window.$ = $; // eslint-disable-line no-multi-assign

// -----------------------------------------------------------------------------
// Main application
// -----------------------------------------------------------------------------

$(() => {
  $('.ponderatedwidget').each((index, elem) => {
    const wrapper = $(elem);
    const remaining = wrapper.find('.ponderatedwidget--remaining');
    const available = wrapper.find('.ponderatedwidget--available');
    const warning = wrapper.find('.ponderatedwidget--warning');
    const error = wrapper.parent().find('.invalid-feedback:contains(mandats disponibles)');
    const inputs = wrapper.find('input');
    $(inputs).on('change', () => {
      const availableInt = parseInt(available[0].textContent, 10);
      const affected = inputs.map((idx, elem) => elem.value).get()
        .reduce((pv, cv) => {
          return pv + (parseInt(cv, 10) || 0);
        }, 0);
      if (availableInt - affected >= 0) {
        remaining[0].textContent = availableInt - affected;
        $(error).addClass('d-none');
        $(warning).addClass('d-none');
      } else {
        $(error).removeClass('d-none');
        $(warning).removeClass('d-none');
      }
    });
  });
});
