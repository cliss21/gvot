from django.urls import path

from . import views

app_name = 'scrutin'
urlpatterns = [
    path('add/', views.ScrutinAdd.as_view(), name='add'),
    path('<int:pk>/answer/', views.ScrutinAnswer.as_view(), name='answer'),
    path(
        '<int:pk>/results/',
        views.ScrutinResults.as_view(),
        name='results'
    ),
    path(
        '<int:pk>/attendees/',
        views.ScrutinAttendees.as_view(),
        name='attendees'
    ),
]
