from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import MaxLengthValidator, MinLengthValidator

from wagtail.contrib.forms.forms import FormBuilder


class LimitedSelectMultiple(forms.SelectMultiple):
    class Media:
        css = {'all': ('css/multi-select.css',)}
        js = ('js/multiselect.js', 'js/jquery.multi-select.js')

    def __init__(self, min_values, max_values, attrs={}, choices=()):
        attrs.update(
            {
                'class': 'limited-multiselect',
                'data-min': min_values,
                'data-max': max_values,
            }
        )
        super().__init__(attrs=attrs, choices=choices)


class LabeledNumberInput(forms.widgets.Input):
    input_type = 'number'
    template_name = 'django/forms/widgets/labeled_number.html'

    def __init__(self, label, *args, **kwargs):
        self.label = label
        super().__init__(*args, **kwargs)

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        context['widget']['label'] = self.label
        return context


class PonderatedWidget(forms.widgets.MultiWidget):
    class Media:
        css = {'all': ('css/ponderated.css',)}
        js = ('js/ponderated.js',)

    template_name = 'django/forms/widgets/ponderatedwidget.html'

    def __init__(self, initials={}, ponderation=1):
        self.ponderation = ponderation
        self.initials = initials
        widgets = [
            LabeledNumberInput(
                label=choice,
                attrs={
                    'value': value,
                    'min': 0,
                    'max': ponderation,
                },
            )
            for choice, value in initials.items()
        ]
        super().__init__(widgets)

    def decompress(self, value):
        if value:
            return [int(s) for s in value.split(',')]
        return []

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        if value is None:
            sum_value = sum(list(self.initials.values()))
        else:
            # value may not be clean data here
            sum_value = 0
            for s in value:
                try:
                    sum_value += int(s)
                except Exception:
                    pass
        if sum_value > self.ponderation:
            sum_value = self.ponderation

        context['widget']['available'] = self.ponderation
        context['widget']['remaining'] = self.ponderation - sum_value
        return context


class PonderatedField(forms.MultiValueField):
    widget = PonderatedWidget

    def __init__(self, initials, ponderation, **kwargs):
        self.ponderation = ponderation
        fields = [
            forms.IntegerField(
                label=choice, initial=value, min_value=0, max_value=ponderation
            )
            for choice, value in initials.items()
        ]
        super().__init__(
            fields, widget=PonderatedWidget(initials, ponderation), **kwargs
        )

    def compress(self, data_list):
        cleaned_data = [i or 0 for i in data_list]
        if self.required and sum(cleaned_data) < self.ponderation:
            raise ValidationError(
                "Vous devez affecter tous les choix disponibles "
                "parmi les options proposées."
            )
        if sum(cleaned_data) > self.ponderation:
            raise ValidationError(
                "Vous ne pouvez pas affecter plus de choix que "
                "le nombre de mandats disponibles."
            )

        return ','.join([str(i) for i in cleaned_data])


class MyFormBuilder(FormBuilder):
    def __init__(self, *args, **kwargs):
        self.pouvoir = kwargs.pop('pouvoir', None)
        super().__init__(*args, **kwargs)

    def create_lim_multiselect_field(self, field, options):
        options['choices'] = map(
            lambda x: (x.strip(), x.strip()), field.choices.split(',')
        )
        validators = []
        if field.min_values:
            validators.append(MinLengthValidator(field.min_values))
        if field.max_values:
            validators.append(MaxLengthValidator(field.max_values))
        return forms.MultipleChoiceField(
            **options,
            validators=validators,
            widget=LimitedSelectMultiple(field.min_values, field.max_values)
        )

    def create_ponderated_field(self, field, options):
        ponderation = getattr(self.pouvoir, 'ponderation', 1)
        choices = field.choices.split(',')
        initials = dict(
            zip(
                choices,
                [
                    ponderation * int(choice == field.default_value)
                    for choice in choices
                ],
            )
        )
        return PonderatedField(
            initials=initials, ponderation=ponderation, **options
        )

    def get_field_options(self, field):
        """
        Default value isn't destinated to PonderatedField, only initials
        is welcome to setup labeled number widgets
        """
        options = super().get_field_options(field)
        if field.field_type == 'ponderated':
            options.pop('initial', None)
        return options
